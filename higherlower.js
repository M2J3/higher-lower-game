let maxNum;
let num;
let listGuesses = [];

function init() {
  do {
    maxNum = Number(prompt("What should be the maximum number?"));
  } while (!isValid(maxNum));

  maxNum = Math.floor(maxNum);
  num = Math.floor(Math.random() * maxNum) + 1;

  let message = document.getElementById("message");
  message.innerHTML = "Guess a number between 1 and " + maxNum;
}

function isValid(input) {
  return !isNaN(input) && input > 0;
}

function do_guess() {
    let guess = Math.round(Number(document.getElementById("guess").value));
    let message = document.getElementById("message");

    if(isNaN(guess)) {
        message.innerHTML = "That is not a number!";
        return;
    }

    if(guess < 1 || guess > maxNum) {
        message.innerHTML = "That number is not in range, try again.";
        return;
    }

    if (listGuesses.includes(guess)) {
        alert("You already guessed that number, try again.");
        return;
      }

    listGuesses.push(guess);

    if(guess == num) {
        let numGuesses = listGuesses.length;
        let guessString = listGuesses.join(", ");
        message.innerHTML = "You got it! It took you " + numGuesses + " tries and your guesses were " + guessString + ".";
    }
    else if (guess > num) {
        message.innerHTML = "No, try a lower number.";
    }
    else {
        message.innerHTML = "No, try a higher number.";
    }
}

window.onload = init;
